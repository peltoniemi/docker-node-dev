FROM node:8-alpine
RUN apk add --update git bash nano

ENTRYPOINT /bin/bash
WORKDIR /app
